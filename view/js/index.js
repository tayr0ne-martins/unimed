
document.addEventListener('input', function () {
    if (document.getElementById('nameFile').lastChild) {
        document.getElementById('nameFile').removeChild(document.getElementById('nameFile').firstChild)
    }
    document.getElementById('nameFile').appendChild(document.createTextNode(document.getElementById('files').files[0].name));
});


let submit = () => {
    var file_data = $('#files').prop('files')[0];
    if(file_data != undefined){
        var form_data = new FormData();
        form_data.append('file', file_data);
        $.ajax({
            url: document.URL + 'controller/mainController.php',
            dataType: 'JSON', 
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (response) {
                console.log(response)
                var json = response
                var arr = [];
                let i = 0
                Object.keys(json).forEach(function (key) {
                    arr.push(json[key]);
                    let str
                    if (i === 0) {
                        str = '<tr>' + json[key].map(item => '<th width="10%">' + item + '</th>') + '<tr>' 
                        i++
                    } else {
                        str = '<tr>' + json[key].map(item => '<td width="10%">' + item + '</td>') + '<tr>'
                    }
                    $("#userTable tbody").append(str);
                });
            }
        });
    }else{
        alert("insira um arquivo");
    }
}

