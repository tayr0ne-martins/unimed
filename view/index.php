<?php include 'layout/layout.php' ?>

<html>
    <head>
        <link rel="icon" href="view/img/s-unimed.png" />
        <title>Unimed</title>
    </head>
    <body>
    <nav class="navbar navbar-dark bg-dark"></nav>
        <div class="container" id="container-upload">
            <div class="row col-12 ">
                <div class="col-sm-9">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="files" >
                            <label class="custom-file-label" id="nameFile" for="inputGroupFile01"></label>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-sm-1">
                    <button type="button" onCLick="submit()" id="buttonSubmit" class="btn btn-dark">Send</button>    
                </div>               
            </div>
        </div>    
        <div class = "container">
            <table class="table table-bordered table-striped" id="userTable">
                <tbody></tbody>
            </table>
        </div>
    </body>
</html> 