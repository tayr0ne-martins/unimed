#Documentação
Instalações necessárias para o sistema funcinar:
	Postegres
	Node
	Php7.2

#Configuração do ambiente:
	Habilitar PDO, no arquivo php.ini e  descomentar a linha  (extension=pdo_pgsql)	;
#Banco de dados:
	Nome da tabela deve se chamar “unimed”
	Necessidade de criar as seguintes tabelas(
	id	
	purchasername
	itemdescription
	merchantaddress
	merchantname
	itemprice
	purchasecount
	)
	Necessário que as colunas sejam criadas exatamente com esses nomes!

#Instalação do sistema:
	A instalação e bem simples, foi desenvolvida um micro framework com um kernel para auxiliar 
A configuração automatizada do sistema!
Abra o projeto no terminal, na raiz do projeto digite o seguinte comando:
	“php unimed  info” com esse comando no terminal será listado todos os comandos que o kernel possui para a manipulação do sistema.
Instalando o sistema apartir do Cli, ainda na raiz do sistema via terminal digite o seguinte comando:
“php unimed install” será instalada toda a dependência do sistema  automaticamente! Pronto !!
Bem simples :D
Agora só falta iniciar o sistema, o kernel foi configurado para iniciar o sistema como um Server próprio!!
Digite o seguinte código no terminal, “php unimed start”, será inicializado o sistema, por padrão o sistema sobe no 127.0.0.1:8000, caso a porta esteja ocupada ou queira subir em uma porta especifica o kernel unimed e inteligente para fazer essa mudança, basta digitar o seguinte comando “php unimed start 9000” o sistema ira subir na porta 9000, assim você pode subir o sistema na porta desejável.


#Qualquer duvida ou complicação na instalação do sistema meu numero é:
Cel:  21981150829   whats app!
