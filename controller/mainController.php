<?php
require('../model/bd.php');
class mainController{
    
    public static function tratarDados($files){
        $tmp_name = $files;//recebe o arquivo para ser manipulado
        $arquivo = $tmp_name;
        $ler=file($arquivo);//abre o arquivo
        $i = 0;        
        $totalBruto = 0;
      
            foreach($ler as $row){
                    $row = explode("\t", $row);
                    $return_arr[] = $row;
                    if($i > 0){
                            if(isset($row[3]) && isset($row[2])){
                                $totalBruto +=  trim($row[3]) * trim($row[2]);  
                            }else{
                                $return_arr[] = ["Erro no formato do arquivo!"]; 
                                break;
                            }
                            try{
                            $save = new OrmUnimed();
                            $save->save($row);
                            }catch (Exception $e) {
                                $return_arr[] = [ $e ] ;
                            }
                    }
                    $i++;  
            }
        $return_arr[] = [("Total gross \t $".number_format($totalBruto,2,'.',','))];
        return $return_arr;
    }
}

echo json_encode(mainController::tratarDados($_FILES["file"]["tmp_name"]));